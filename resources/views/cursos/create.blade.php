@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('cursos') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('cursos.store') }}" aria-label="{{ __('Cadastro') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="curso_descricao" class="col-md-4 col-form-label text-md-right">{{ __('Nome') }}</label>

                            <div class="col-md-6">
                                <input id="curso_descricao" type="text" class="form-control" name="curso_descricao"  required autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="curso_chs" class="col-md-4 col-form-label text-md-right">{{ __('CHS') }}</label>

                            <div class="col-md-6">
                                <input id="curso_chs" type="number" class="form-control" name="curso_chs" placeholder="em horas" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="curso_tempomax" class="col-md-4 col-form-label text-md-right">{{ __('Tempo maximo') }}</label>

                            <div class="col-md-6">
                                <input id="curso_tempomax" type="number" class="form-control" name="curso_tempomax" placeholder="em anos" required autofocus>

                            </div>

                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
