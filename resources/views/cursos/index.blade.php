@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <table class="table table-hover">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Codigo</th>
                    <th scope="col">Nome</th>
                    <th scope="col">CHS</th>
                    <th scope="col">Tempo Máximo</th>
                </tr>
                </thead>

                <tbody>
                @foreach($cursos as $curso)
                <tr>
                    <th scope="row">{{$curso->id}}</th>
                    <td>{{$curso->curso_codigo}}</td>
                    <td>{{$curso->curso_descricao}}</td>
                    <td>{{$curso->curso_chs}}</td>
                    <td>{{$curso->curso_tempomax}}</td>

                </tr>
                @endforeach
                </tbody>
            </table>
            <a class="btn btn-primary btn-md btn-block" href="{{route('cursos.create')}}">Adicionar novo</a>

        </div>
    </div>
</div>
@endsection
