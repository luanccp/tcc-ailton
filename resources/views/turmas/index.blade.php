@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <table class="table table-hover">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Disciplina</th>
                    <th scope="col">Professor</th>
                    <th scope="col">Vagas</th>
                </tr>
                </thead>

                <tbody>
                @foreach($turmas as $turma)
                <tr>
                    <th scope="row">{{$turma->id}}</th>
                    <td>
                        @foreach($disciplinas as $disciplina)
                            @if($disciplina->id == $turma->disc_id)
                                {{$disciplina->disc_descricao}}
                            @endif
                        @endforeach</td>
                    <td>
                        @foreach($professores as $professor)
                            @if($professor->id == $turma->professor_id)
                                {{$professor->professor_nome}}
                            @endif
                        @endforeach
                    </td>
                    <td>{{$turma->turma_vagas}}</td>

                </tr>
                @endforeach
                </tbody>
            </table>
            <a class="btn btn-primary btn-md btn-block" href="{{route('turmas.create')}}">Adicionar novo</a>

        </div>
    </div>
</div>
@endsection
