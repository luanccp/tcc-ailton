@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('turmas') }}</div>

                    <div class="card-body">
                        {!! Form::open(array('route'=>'turmas.store', 'id'=>'frmsave', 'method'=>'POST')) !!}
                        @csrf
                        <div class="form-group row">
                            <label for="turma_vagas"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Disciplina') }}</label>

                            <div class="col-md-6">
                                <select class="custom-select" name="disc_id">
                                    <option selected>Open this select menu</option>
                                    @foreach($disciplinas as $disciplina)
                                        <option value="{{$disciplina->id}}">{{$disciplina->disc_descricao}}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label for="turma_vagas"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Professor') }}
                            </label>


                            <div class="col-md-6">
                                <select class="custom-select" name="professor_id">
                                    <option selected>Open this select menu</option>
                                    @foreach($professores as $professor)
                                        <option value="{{$professor->id}}">{{$professor->professor_nome}}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label for="turma_vagas"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Vagas') }}</label>

                            <div class="col-md-6">
                                <input id="turma_vagas" type="number" class="form-control" name="turma_vagas" required
                                       autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="turma_horario"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Horarios') }}</label>
                            <a class="btn btn-primary" href="javascript:void(0)" id="addInput">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                Adicionar aula
                            </a>
                            <div class="container">

                                <br/>
                                <div id="dynamicDiv" class="row">
                                    <div class="col-md-4">
                                        <input type="text" id="inputeste" class="form-control"
                                               placeholder="dia da semana" name="aula_dia[]"/>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" id="inputeste" class="form-control" placeholder="Hora de inicio" name="aula_hora_inicio[]"/>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" id="inputeste" class="form-control" placeholder="Hora de fim"
                                               name="aula_hora_fim[]"/>
                                    </div>

                                </div>

                                <script>
                                    $(function () {
                                        var scntDiv = $('#dynamicDiv');
                                        $(document).on('click', '#addInput', function () {
                                            $('<div class="container">' +
                                                '<div id="lastDiv" class="row">\n' +
                                                '<div class="col-md-4">\n' +
                                                '<input type="text" id="inputeste" class="form-control" placeholder="dia da semana" name="aula_dia[]"/>\n' +
                                                '</div>\n' +
                                                '<div class="col-md-3">\n' +
                                                '<input type="text" id="inputeste" class="form-control" placeholder="Hora de inicio" name="aula_hora_inicio[]"/></div>\n' +
                                                '<div class="col-md-3">\n' +
                                                '<input type="text" id="inputeste" class="form-control" placeholder="Hora de fim" name="aula_hora_fim[]"/>\n' +
                                                '</div>\n' +
                                                '<div class="col-md-2">\n' +
                                                '<a class="btn btn-danger" href="javascript:void(0)" id="remInput">X</a>' +
                                                '</div>' +
                                                '</div>').appendTo(scntDiv);
                                            return false;
                                        });
                                        $(document).on('click', '#remInput', function () {
                                            $(this).parents('#lastDiv').remove();
                                            return false;
                                        });
                                    });
                                </script>
                            </div>

                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {!! Form::submit('Cadastrar', array('class'=>'btn btn-primary btn-default')) !!}
                            </div>
                        </div>
                        {!! Form::hidden('_token', csrf_token()) !!}
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
