
@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">{{ __('disciplinas') }}</div>

				<div class="card-body">
					<form method="POST" action="{{ route('disciplinas.store') }}" aria-label="{{ __('Cadastro') }}" novalidate>
						@csrf

						<div class="form-group row">
							<label for="disc_descricao" class="col-md-4 col-form-label text-md-right">{{ __('Nome') }}</label>

							<div class="col-md-6">
								<input id="disc_descricao" type="text" class="form-control" name="disc_descricao"  required autofocus>
							</div>
						</div>
						<div class="form-group row">
							<label for="disc_chs" class="col-md-4 col-form-label text-md-right">{{ __('CHS') }}</label>

							<div class="col-md-6">
								<input id="disc_chs" type="text" class="form-control" name="disc_chs"  required autofocus>
							</div>
						</div>
						<div class="form-group row">
							<label for="disc_chs"class="col-md-4 col-form-label text-md-right">{{ __('Area') }}</label>

							<div class="col-md-6">
								<input id="disc_area" type="text" class="form-control" name="disc_area"  required autofocus>
							</div>
						</div>

						<div class="form-group row">
							<label for="disc_indice_reprovacao" class="col-md-4 col-form-label text-md-right">{{ __('Indicie de reprovação') }}</label>

							<div class="col-md-6">
								<input id="disc_indice_reprovacao" type="text" class="form-control" name="disc_indice_reprovacao"  required autofocus>
							</div>
						</div>

						<div class="form-group row">
							<label for="disc_periodo" class="col-md-4 col-form-label text-md-right">{{ __('Periodo') }}</label>

							<div class="col-md-6">
								<input id="disc_periodo" type="text" class="form-control" name="disc_periodo"  required autofocus>
							</div>
						</div>

						<div class="form-group row">
							<label for="departamento_id" class="col-md-4 col-form-label text-md-right">{{ __('Departamento') }}</label>
							<div class="col-md-6">
								<select class="custom-select" name="departamento_id">
									<option selected>Selecione o departamento</option>
									@foreach($departamentos as $departamento)
									<option value="{{$departamento->id}}">{{$departamento->dep_descricao}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="disc_optativa" class="col-md-4 col-form-label text-md-right">{{ __('Optativa') }}</label>

							<div class="col-md-6">
								<input id="disc_optativa" type="checkbox" class="form-control" name="disc_optativa"   autofocus>
							</div>
						</div>
						<div class="form-group row">
							<label for="prereq"
							class="col-md-4 col-form-label text-md-right">{{ __('Pre-requisitos') }}</label>
							<a class="btn btn-primary" href="javascript:void(0)" id="addInput">
								<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
								Adicionar pre requisito
							</a>
							<div class="container">
								<br/>
								<div id="dynamicDiv" class="row">
									<div class="col-md-5">
										<select id="select1" class="custom-select" name="prereq_tipo" onchange="mostrarDisciplinas(this.id)">
											<option selected>Selecione o tipo do pre requisito</option>
											<option value="Horas">Horas</option>
											<option value="Disciplina">Disciplina</option>
										</select>
									</div>
									<div class="col-md-5">
										<select id="select2" class="custom-select" name="prereq_valor" hidden=true>
											<option selected>Selecione a disciplina</option>
											@foreach($disciplinas as $disciplina)
											<option value="{{$disciplina->id}}">{{$disciplina->disc_descricao}}</option>
											@endforeach
										</select>

										<input id="horas" type="number" class="form-control" name="prereq_valor" hidden=true required autofocus>
									</div>

									<script>
									function mostrarDisciplinas(s1) {
											var s1 = document.getElementById(s1);
											if (s1.value == "Disciplina") {
												$('#select2').attr('hidden', false);
												$('#horas').attr('hidden', true)
											}
											else if (s1.value == "Horas") {
												$('#horas').attr('hidden', false)
												$('#select2').attr('hidden', true);
											}
									}
									$(function () {
										var scntDiv = $('#dynamicDiv');
										$(document).on('click', '#addInput', function () {
											$('<div class="container">' +
											'<div id="lastDiv" class="row">\n' +
											'<div class="col-md-5">\n' +
											'<input type="text" id="inputeste" class="form-control" placeholder="Horas ou disciplina" name="prereq_tipo[]"/>\n' +
											'</div>\n' +
											'<div class="col-md-5">\n' +
											'<input type="text" id="inputeste" class="form-control" placeholder="Nome da disciplina" name="prereq_valor[]"/></div>\n' +
											'<div class="col-md-2">\n' +
											'<a class="btn btn-danger" href="javascript:void(0)" id="remInput">X</a>' +
											'</div>' +
											'</div>').appendTo(scntDiv);
											return false;
										});
										$(document).on('click', '#remInput', function () {
											$(this).parents('#lastDiv').remove();
											return false;
										});
									});
									</script>
								</div>

							</div>
						</div>

						<div class="form-group row mb-0">
							<div class="col-md-6 offset-md-4">
								<button type="submit" class="btn btn-primary">
									{{ __('Register') }}
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
