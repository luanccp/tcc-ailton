@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <table class="table table-hover">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Codigo</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Periodo</th>
                    <th scope="col">CHS</th>
                    <th scope="col">Optativa</th>
                    <th scope="col">Indice reprovacao</th>
                </tr>
                </thead>

                <tbody>
                @foreach($disciplinas as $disciplina)
                <tr>
                    <th scope="row">{{$disciplina->id}}</th>
                    <td>{{$disciplina->disc_codigo}}</td>
                    <td>{{$disciplina->disc_descricao}}</td>
                    <td>{{$disciplina->disc_periodo}}</td>
                    <td>{{$disciplina->disc_chs}}</td>
                    <td>{{$disciplina->disc_optativa}}</td>
                    <td>{{$disciplina->disc_indice_reprovacao}}%</td>

                </tr>
                @endforeach
                </tbody>
            </table>
            <a class="btn btn-primary btn-md btn-block" href="{{route('disciplinas.create')}}">Adicionar novo</a>

        </div>
    </div>
</div>
@endsection
