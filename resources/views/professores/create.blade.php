@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('professores') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('professores.store') }}" aria-label="{{ __('Cadastro') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="professor_nome" class="col-md-4 col-form-label text-md-right">{{ __('Nome') }}</label>

                                <div class="col-md-6">
                                    <input id="professor_nome" type="text" class="form-control" name="professor_nome"  required autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="professor_sobrenome" class="col-md-4 col-form-label text-md-right">{{ __('Sobrenome') }}</label>

                                <div class="col-md-6">
                                    <input id="professor_sobrenome" type="text" class="form-control" name="professor_sobrenome"  required autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="professor_indice_reprovacao" class="col-md-4 col-form-label text-md-right">{{ __('Indicie de reprovação') }}</label>

                                <div class="col-md-6">
                                    <input id="professor_indice_reprovacao" type="text" class="form-control" name="professor_indice_reprovacao"  required autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="professor_especialidade" class="col-md-4 col-form-label text-md-right">{{ __('Especialidade') }}</label>

                                <div class="col-md-6">
                                    <input id="professor_especialidade" type="text" class="form-control" name="professor_especialidade"  required autofocus>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
