@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <table class="table table-hover">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Sobrenome</th>
                    <th scope="col">Reprovação</th>
                    <th scope="col">Especialidade</th>
                </tr>
                </thead>

                <tbody>
                @foreach($professores as $professor)
                <tr>
                    <th scope="row">{{$professor->id}}</th>
                    <td>{{$professor->professor_nome}}</td>
                    <td>{{$professor->professor_sobrenome}}</td>
                    <td>{{$professor->professor_indice_reprovacao}} %</td>
                    <td>{{$professor->professor_especialidade}}</td>

                </tr>
                @endforeach
                </tbody>
            </table>
            <a class="btn btn-primary btn-md btn-block" href="{{route('professores.create')}}">Adicionar novo</a>

        </div>
    </div>
</div>
@endsection
