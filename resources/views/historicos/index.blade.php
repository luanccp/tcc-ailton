@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <table class="table table-hover">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Turma</th>
                    <th scope="col">Nota</th>
                    <th scope="col">Status</th>
                </tr>
                </thead>

                <tbody>
                @foreach($historicos as $historico)
                <tr>
                    <th scope="row">{{$historico->id}}</th>
                    <td>{{$historico->hist_turma}}</td>
                    <td>{{$historico->hist_nota}}</td>
                    <td>{{$historico->hist_status}}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
            <a class="btn btn-primary btn-md btn-block" href="{{route('historicos.create')}}">Adicionar novo</a>

        </div>
    </div>
</div>
@endsection
