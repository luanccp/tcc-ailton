@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('historicos') }}</div>

                    <div class="card-body">
                        {!! Form::open(array('route'=>'historicos.store', 'id'=>'frmsave', 'method'=>'POST')) !!}
                        @csrf
                        <div class="form-group row">
                            <label for="turma_id"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Turma') }}</label>

                            <div class="col-md-6">
                                <select class="custom-select" name="disc_id">
                                    <option selected>Open this select menu</option>
                                    @foreach($turmas as $turma)
                                  
                                    <option value="{{$turma->id}}">{{$turma->disc_id}}</option>  
                                  
                                    @endforeach
                                </select>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label for="hist_nota"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Nota') }}</label>

                            <div class="col-md-6">
                                <input id="hist_nota" type="number" class="form-control" name="hist_nota" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="hist_status"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>

                            <div class="col-md-6">
                                <input id="hist_status" type="text" class="form-control" name="hist_status" required
                                       autofocus>
                            </div>
                        </div>
                        
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {!! Form::submit('Cadastrar', array('class'=>'btn btn-primary btn-default')) !!}
                            </div>
                        </div>
                        {!! Form::hidden('_token', csrf_token()) !!}
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
