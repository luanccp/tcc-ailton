@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('departamentos') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('departamentos.store') }}" aria-label="{{ __('Cadastro') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="dep_descricao" class="col-md-4 col-form-label text-md-right">{{ __('Nome') }}</label>

                            <div class="col-md-6">
                                <input id="dep_descricao" type="text" class="form-control" name="dep_descricao"  required autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="dep_sigla" class="col-md-4 col-form-label text-md-right">{{ __('Sigla') }}</label>

                            <div class="col-md-6">
                                <input id="dep_sigla" type="text" class="form-control" name="dep_sigla"  required autofocus>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
