@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <table class="table table-hover">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Codigo</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Sigla</th>
                </tr>
                </thead>

                <tbody>
                @foreach($departamentos as $departamento)
                <tr>
                    <th scope="row">{{$departamento->id}}</th>
                    <td>{{$departamento->dep_codigo}}</td>
                    <td>{{$departamento->dep_descricao}}</td>
                    <td>{{$departamento->dep_sigla}}</td>

                </tr>
                @endforeach
                </tbody>
            </table>
            <a class="btn btn-primary btn-md btn-block" href="{{route('departamentos.create')}}">Adicionar novo</a>

        </div>
    </div>
</div>
@endsection
