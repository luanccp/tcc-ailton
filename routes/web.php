<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix'=>'departamentos'], function (){
    Route::get('', ['as' => 'departamentos.index', 'uses' => 'DepartamentoController@index']);
    Route::get('create' ,['as' => 'departamentos.create','uses'  => 'DepartamentoController@create']);
    Route::post('store' ,['as' => 'departamentos.store','uses'  => 'DepartamentoController@store']);

});

Route::group(['prefix'=>'cursos'], function (){
    Route::get('', ['as' => 'cursos.index', 'uses' => 'CursoController@index']);
    Route::get('create' ,['as' => 'cursos.create','uses'  => 'CursoController@create']);
    Route::post('store' ,['as' => 'cursos.store','uses'  => 'CursoController@store']);

});

Route::group(['prefix'=>'disciplinas'], function (){
    Route::get('', ['as' => 'disciplinas.index', 'uses' => 'DisciplinaController@index']);
    Route::get('create' ,['as' => 'disciplinas.create','uses'  => 'DisciplinaController@create']);
    Route::post('store' ,['as' => 'disciplinas.store','uses'  => 'DisciplinaController@store']);
});

Route::group(['prefix'=>'professores'], function (){
    Route::get('', ['as' => 'professores.index', 'uses' => 'ProfessorController@index']);
    Route::get('create' ,['as' => 'professores.create','uses'  => 'ProfessorController@create']);
    Route::post('store' ,['as' => 'professores.store','uses'  => 'ProfessorController@store']);

});
Route::group(['prefix'=>'turmas'], function (){
    Route::get('', ['as' => 'turmas.index', 'uses' => 'TurmaController@index']);
    Route::get('create' ,['as' => 'turmas.create','uses'  => 'TurmaController@create']);
    Route::post('store' ,['as' => 'turmas.store','uses'  => 'TurmaController@store']);

});

Route::group(['prefix'=>'historicos'], function (){
    Route::get('', ['as' => 'historicos.index', 'uses' => 'HistoricoController@index']);
    Route::get('create' ,['as' => 'historicos.create','uses'  => 'HistoricoController@create']);
    Route::post('store' ,['as' => 'historicos.store','uses'  => 'HistoricoController@store']);

});