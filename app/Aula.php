<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aula extends Model
{
    protected $fillable =[
        'aula_dia',
        'aula_hora_inicio',
        'aula_hora_fim',
        'turma_id'
    ];
}
