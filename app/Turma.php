<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turma extends Model
{
    protected $fillable = [
        'turma_codigo',
        'turma_vagas',
        'professor_id',
        'disc_id'
    ];

    public function Aulas(){
        return $this->hasMany('App\Aula');
    }

    public function User(){
        return $this->belongsToMany('App\User', 'alunos_has_turmas');
    }
}
