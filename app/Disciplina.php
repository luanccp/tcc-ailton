<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disciplina extends Model
{
    protected $fillable = [
        'disc_codigo',
        'disc_descricao',
        'disc_area',
        'disc_periodo',
        'disc_chs',
        'disc_optativa',
        'disc_indice_reprovacao',
        'departamento_id'
    ];

    public function Turmas(){
        return $this->hasMany('App\Turma');
    }
    public function Historico(){
        return $this->belongsToMany('App\Historico', 'disciplinas_has_historico');
    }
}
