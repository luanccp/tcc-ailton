<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historico extends Model
{
    protected $fillable = [
        'user_id',
        'turma_id',
        'hist_nota',
        'hist_status'
    ];

    public function User(){
        return $this->hasOne('App\User');
    }

    public function Turma(){
        return $this->hasMany('App\Turma');
    }

    public function Disciplina(){
        return $this->belongsToMany('App\Disciplina', 'disciplinas_has_historico');
    }
}
