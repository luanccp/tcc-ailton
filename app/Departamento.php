<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $fillable =[
        'dep_codigo',
        'dep_descricao',
        'dep_sigla'
    ];
}
