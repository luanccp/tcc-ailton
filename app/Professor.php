<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Professor extends Model
{
    protected $fillable = [
        'professor_nome',
        'professor_sobrenome',
        'professor_indice_reprovacao',
        'professor_especialidade'
    ];

    public function Turmas(){
        return $this->hasMany('App\Turma');
    }
}
