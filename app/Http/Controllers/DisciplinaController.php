<?php

namespace App\Http\Controllers;

use App\Disciplina;
use App\Departamento;
use App\PreRequisito;
use Illuminate\Http\Request;

class DisciplinaController extends Controller
{
    private $disciplina;
    private $departamento;

    public function __construct(Disciplina $disciplina, Departamento $departamento)
    {
        $this->disciplina =$disciplina;
        $this->departamento =$departamento;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $disciplinas = $this->disciplina->all();
        $departamentos = $this->departamento->all();
        return view('disciplinas.index',compact('disciplinas', 'departamentos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departamentos = Departamento::get();
        $disciplinas = Disciplina::get();
        return view('disciplinas.create')->with( compact('departamentos','disciplinas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request['disc_optativa'] == 'on') $request['disc_optativa'] = 1;
        else $request['disc_optativa'] = 0;

        $this->disciplina->create([
            'disc_codigo' => rand(0,100),
            'disc_descricao' => $request['disc_descricao'],
            'disc_chs' => $request['disc_chs'],
            'disc_periodo' => $request['disc_periodo'],
            'disc_optativa' => $request['disc_optativa'],
            'disc_indice_reprovacao' => $request['disc_indice_reprovacao'],
            'disc_area' => $request['disc_area'],
            'departamento_id' => $request['departamento_id'],
        ]);

        //CASO SEJA DO TIPO DISCIPLINA
        if ($request['prereq_tipo'] == "Disciplina") {
          $id = $request['prereq_valor'];
          //$valor = \App\Disciplina::find($id)->disc_descricao;
          $valor=null;
        }
        else {
          $id = null;
          $valor = $request['prereq_valor'];
        }

        $prerequisito = new PreRequisito();
        $prerequisito->prereq_tipo = $request['prereq_tipo'];
        $prerequisito->prereq_valor = $valor;
        $prerequisito->disc_id = $id;
        $prerequisito->save();
        // $this->prerequisitos->create([
        //   'prereq_tipo'=>$request['prereq_tipo'],
        //   'prereq_valor'=>$valor,
        //   'disc_id'=> $id
        // ]);


        return redirect()->route('disciplinas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Disciplina  $disciplina
     * @return \Illuminate\Http\Response
     */
    public function show(Disciplina $disciplina)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Disciplina  $disciplina
     * @return \Illuminate\Http\Response
     */
    public function edit(Disciplina $disciplina)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Disciplina  $disciplina
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Disciplina $disciplina)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Disciplina  $disciplina
     * @return \Illuminate\Http\Response
     */
    public function destroy(Disciplina $disciplina)
    {
        //
    }
}
