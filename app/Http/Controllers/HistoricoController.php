<?php

namespace App\Http\Controllers;
use App\Disciplina;
use App\Historico;
use App\Turma;
use Illuminate\Http\Request;

class HistoricoController extends Controller
{
    private $historico;
    private $disciplina;
    private $turma;

    public function __construct(Historico $historico, Disciplina $disciplina, Turma $turma)
    {
        $this->historico = $historico;
        $this->disciplina = $disciplina;
        $this->turma = $turma;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $historicos = $this->historico->all();
        $turmas = $this->turma->all();
        return view('historicos/index', compact('historicos', 'turmas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $turmas = Turma::get();
        //$result = Disciplina::with('turmas')->get();
        //$disciplina = DB::table('disciplinas')->where('id', $turma->disc_id);
        return view('historicos.create', compact('turmas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->historico->create([]
            'user_id' => 1,
            'turma_id' => $request->['turma_id'],
            'hist_nota' => $request->['hist_nota'],
            'hist_status' = > $request->['hist_status']



        ]);
        return redirect()->route('historico.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Historico  $historico
     * @return \Illuminate\Http\Response
     */
    public function show(Historico $historico)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Historico  $historico
     * @return \Illuminate\Http\Response
     */
    public function edit(Historico $historico)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Historico  $historico
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Historico $historico)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Historico  $historico
     * @return \Illuminate\Http\Response
     */
    public function destroy(Historico $historico)
    {
        //
    }
}
