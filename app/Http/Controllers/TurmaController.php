<?php

namespace App\Http\Controllers;

use App\Aula;
use App\Disciplina;
use App\Professor;
use App\Turma;
use Illuminate\Http\Request;

class TurmaController extends Controller
{
    private $turma;
    private $professor;

    public function __construct(Turma $turma, Professor $professor, Disciplina $disciplina)
    {
        $this->turma = $turma;
        $this->professor = $professor;
        $this->disciplina = $disciplina;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $turmas = $this->turma->all();
        $professores = $this->professor->all();
        $disciplinas = $this->disciplina->all();
        return view('turmas.index', compact('turmas','professores','disciplinas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $professores=Professor::get();
        $disciplinas=Disciplina::get();
        $aulas=Aula::pluck('id');
        return view('turmas.create')->with(compact('professores','disciplinas','aulas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $codigo = rand(0,100);
        $this->turma->create([
            'turma_codigo' => $codigo,
            'turma_vagas' => $request['turma_vagas'],
            'professor_id' => $request['professor_id'],
            'disc_id' => $request['disc_id']
        ]);

        $input = $request->all();
        $aulaAux = $request->input('aula_dia');
        $aulaHoraInicio = $request->input('aula_hora_inicio');
        $aulaHoraFim = $request->input('aula_hora_fim');
        for($i=0; $i< count($request['aula_dia']); $i++) {
            $teste = new Aula();
            $teste->turma_codigo = $codigo;
            $teste->aula_dia = $aulaAux[$i];
            $teste->aula_hora_inicio = $aulaHoraInicio[$i];
            $teste->aula_hora_fim = $aulaHoraFim[$i];
            $teste->save();
        }

        return redirect()->route('turmas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Turma  $turma
     * @return \Illuminate\Http\Response
     */
    public function show(Turma $turma)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Turma  $turma
     * @return \Illuminate\Http\Response
     */
    public function edit(Turma $turma)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Turma  $turma
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Turma $turma)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Turma  $turma
     * @return \Illuminate\Http\Response
     */
    public function destroy(Turma $turma)
    {
        //
    }
}
