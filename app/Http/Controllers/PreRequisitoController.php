<?php

namespace App\Http\Controllers;

use App\PreRequisito;
use Illuminate\Http\Request;

class PreRequisitoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PreRequisito  $preRequisito
     * @return \Illuminate\Http\Response
     */
    public function show(PreRequisito $preRequisito)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PreRequisito  $preRequisito
     * @return \Illuminate\Http\Response
     */
    public function edit(PreRequisito $preRequisito)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PreRequisito  $preRequisito
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PreRequisito $preRequisito)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PreRequisito  $preRequisito
     * @return \Illuminate\Http\Response
     */
    public function destroy(PreRequisito $preRequisito)
    {
        //
    }
}
