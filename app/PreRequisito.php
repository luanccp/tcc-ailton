<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreRequisito extends Model
{
    protected $fillable = [
        'prereq_tipo',
        'prereq_valor',
        'disc_id'
    ];

    public function Disciplina(){
        return $this->belongsTo('App\Disciplina');
    }
}
