<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    protected $fillable = [
      'curso_codigo',
      'curso_descricao',
      'curso_chs',
      'curso_tempomax'
    ];

    public function Users(){
        return $this->hasMany('App\User');
    }
}
