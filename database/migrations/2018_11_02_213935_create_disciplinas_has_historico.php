<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisciplinasHasHistorico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disciplinas_has_historico', function (Blueprint $table) {
            $table->integer('disc_id')->unsigned();
            $table->foreign('disc_id')->references('id')->on('disciplinas');
            $table->integer('hist_id')->unsigned();
            $table->foreign('hist_id')->references('id')->on('historicos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disciplinas_has_historico');
    }
}
