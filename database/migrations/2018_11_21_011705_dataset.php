<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Dataset extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dataset', function (Blueprint $table) {
            $table->increments('id');
            $table->string('perfil');
            $table->string('disc');
            $table->string('apt_mat');
            $table->string('apt_comp');
            $table->string('apt_cn');
            $table->string('apt_ch');
            $table->string('apt_ele');
            $table->string('disc_ind_aprovacao');
            $table->string('prof_ind_aprovacao');
            $table->string('status');
            $table->string('tentativa');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dataset');
    }
}
