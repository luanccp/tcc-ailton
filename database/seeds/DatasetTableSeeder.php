<?php

use Illuminate\Database\Seeder;

class DatasetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $areas = array(
            "Matematica",
            "Computação",
            "Ciencias da natureza",
            "Ciencias Humanas",
            "Elétrica"
        );
        $disciplina_cont = 0;
        $user_cont = 0;
        $disciplinas = DB::table('disciplinas')->get();
        for ($i=0; $i <30000 ; $i++)
        {
            $indice_ap = 0;
            $disc = $disciplinas[$disciplina_cont]->disc_descricao;
            $disc_ind_aprovacao = rand(2, 9) / 10;
            $prof_ind_aprovacao = rand(2, 9) / 10;
            $apt1 = rand(2, 10) / 10;
            $apt2 = rand(2, 10) / 10;
            $apt3 = rand(2, 10) / 10;
            $apt4 = rand(2, 10) / 10;
            $apt5 = rand(2, 10) / 10;

            //ESCOLHA DA APTIDAO DO ALUNO
            if ($apt1>=$apt2 && $apt1>=$apt3 && $apt1>=$apt4 && $apt1>=$apt5) {
                $indice_perfil = 0;
                $perfil = $areas[0];
                $indice_perfil = $apt1/3;
            }
            if ($apt2>=$apt1 && $apt2>=$apt3 && $apt2>=$apt4 && $apt2>=$apt5){
                $indice_perfil = 0;
                $perfil = $areas[1];
                $indice_perfil = $apt2/3;
            }
            if ($apt3>=$apt1 && $apt3>=$apt2 && $apt3>=$apt4 && $apt3>=$apt5) {
                $indice_perfil = 0;
                $perfil = $areas[2];
                $indice_perfil = $apt3/3;
            }
            if ($apt4>=$apt1 && $apt4>=$apt2 && $apt4>=$apt3 && $apt4>=$apt5) {
                $indice_perfil = 0;
                $perfil = $areas[3];
                $indice_perfil = $apt4/3;
            }
            if ($apt5>=$apt1 && $apt5>=$apt2 && $apt5>=$apt3 && $apt5>=$apt4) {
                $indice_perfil = 0;
                $perfil = $areas[4];
                $indice_perfil = $apt5/3;
            }

            //ITERAÇÕES DO ALUNO COM A MESMA DISCIPLINA, ATE ELE SER  APROVADO
            $flag = true;
            for ($j=0; $flag ; $j++)
            {
                //PRIMEIRA VEZ TENTANDO A DISCIPLINA
                if ($j == 0) {
                    //COMPATIBILIDADE COM PERFIL
                    if ($disciplinas[$disciplina_cont]->disc_area == $perfil) {
                        $indice_ap = $indice_perfil;
                    }
                    //FATOR PROFESSOR + DISCIPLINA
                    $indice_ap = $indice_ap + (($disc_ind_aprovacao + $prof_ind_aprovacao)/3);
                }
                //NAO E A PRIMEIRA VEZ QUE TENTA A DISCIPLINAS
                else if ($j == 1) {
                    $indice_ap = $indice_ap + 0.1;
                }
                else{
                    $indice_ap = $indice_ap + 0.2;
                }
                //CONDIÇÃO DE PARADA
                if ($indice_ap >= 0.5) {
                    $flag = false;
                    $disciplina_cont++;
                }
            }
            //SALVA O REGISTRO NO BANCO
            DB::table('dataset')->insert([
                'perfil' => $perfil,
                'disc' => $disc,
                'apt_mat' => $apt1,
                'apt_comp' => $apt2,
                'apt_cn' => $apt3,
                'apt_ch' => $apt4,
                'apt_ele' => $apt5,
                'disc_ind_aprovacao' => $disc_ind_aprovacao,
                'prof_ind_aprovacao' => $prof_ind_aprovacao,
                'status' => $indice_ap,
                'tentativa' =>$j
            ]);

            //RODOU O ALUNO INTEIRO
            if ($disciplina_cont == 56) {
                $disciplina_cont = 0;
            }
        }
    }
}
