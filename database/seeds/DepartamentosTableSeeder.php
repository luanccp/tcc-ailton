<?php

use Illuminate\Database\Seeder;

class DepartamentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contador = 0;
        $departamentos = array(
            "Departamento de Computação e Eletronica",
            "Departamento de Matematica Aplicada",
            "Departamento de Ciencias Naturais",
            "Departamento de Engenharias e Tecnologia",
            "Departamento de Educação e Ciências Humanas",
        );
        $siglas = array(
            'DECEL',
            'DMA',
            'DCN',
            'DETEC',
            'DECH'
        );
        while ($contador < count($departamentos)) {
            DB::table('departamentos')->insert([
                'dep_codigo' =>str_random(5),
                'dep_descricao' => $departamentos [$contador],
                'dep_sigla' => $siglas[$contador],
            ]);
            $contador++;
        }
    }
}
