<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
    * Seed the application's database.
    *
    * @return void
    */
    public function run()
    {
        // factory(App\User::class, 1000)->create();
        $this->call([
            // DepartamentosTableSeeder::class,
            // CursosTableSeeder::class,
            // DisciplinasTableSeeder::class,
            // TurmasTableSeeder::class,
            DatasetTableSeeder::class
        ]);

        // factory(App\Curso::class, 12)->create();
        // factory(App\Departamento::class, 5, $contador)->create();
        // factory(App\Professor::class, 20)->create();
        // factory(App\User::class, 1000)->create();
        // factory(App\Disciplina::class, 200)->create();
        // factory(App\Turma::class, 5)->create();
        // factory(App\Historico::class, 5)->create();
    }
}
