<?php

use Illuminate\Database\Seeder;

class TurmasTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $contador = 0;
        $vagas = array(20,40,50,15);
        $disciplinas = DB::table('disciplinas')->get();
        $professores = DB::table('professors')->get();


        while ($contador < count($disciplinas)) {
            DB::table('turmas')->insert([
                'turma_codigo' =>str_random(5),
                'turma_vagas' => $vagas[rand(0,3)],
                'professor_id' => $professores[rand(0,19)]->id,
                'disc_id' => $disciplinas[$contador]->id,
            ]);
            $contador++;
        }
    }
}
