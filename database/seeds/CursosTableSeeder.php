<?php

use Illuminate\Database\Seeder;

class CursosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contador = 0;
        $cursos = array(
            'Engenharia da computação',
            'Engenharia Quimica',
            'Engenharia de Producao',
            'Engenharia de Petroleo',
        );
        while ($contador < count($cursos)) {
            DB::table('cursos')->insert([
                'curso_codigo' =>str_random(2).rand(0,999),
                'curso_descricao' => $cursos[$contador],
                'curso_chs' => rand(2900,3500),
                'curso_tempomax' =>rand(4,9),
            ]);
            $contador++;
        }
    }
}
