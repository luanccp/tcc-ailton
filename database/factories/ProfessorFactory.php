<?php

use Faker\Generator as Faker;

$factory->define(App\Professor::class, function (Faker $faker) {
    $areas = array(
        "Matematica",
        "Computação",
        "Ciencias da natureza",
        "Ciencias Humanas",
        "Elétrica"
    );

    return [
        'professor_nome' =>$faker->firstName,
        'professor_sobrenome' => $faker->lastName,
        'professor_indice_reprovacao' => rand(0,100),
        'professor_especialidade' =>$areas[rand(0,4)],
    ];
});
