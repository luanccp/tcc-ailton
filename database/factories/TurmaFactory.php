<?php

use Faker\Generator as Faker;

$factory->define(App\Turma::class, function (Faker $faker) {
    return [
      'turma_codigo' => rand(1,10),
      'turma_vagas' => rand(40,50),
      'professor_id' => rand(1,5),
      'disc_id'=> rand(1,10),      
        //
    ];
});
