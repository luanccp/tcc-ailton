<?php

use Faker\Generator as Faker;

$factory->define(App\Curso::class, function (Faker $faker) {
    return [
        'curso_codigo' =>str_random(2).rand(0,999),
        'curso_descricao' => $faker->company,
        'curso_chs' => rand(2900,3500),
        'curso_tempomax' =>rand(4,9),
    ];
});
