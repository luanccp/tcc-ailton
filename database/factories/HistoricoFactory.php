<?php

use Faker\Generator as Faker;

$factory->define(App\Historico::class, function (Faker $faker) {
  $status = null;
  $nota = rand(0,10);


  if ($nota>=5) $status = "Aprovado";
  else $status = "Reprovado";




    return [
      'user_id' => 1,
      'turma_id'=>rand(1,5),
      'hist_nota' =>$nota,
      'hist_status' =>$status,
    ];
});
