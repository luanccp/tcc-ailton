<?php

use Faker\Generator as Faker;

$factory->define(App\Departamento::class, function (Faker $faker) {

    return [
       'dep_codigo' =>str_random(5),
       'dep_descricao' => $faker->company,
       'dep_sigla' => $faker->stateAbbr,
   ];
});
