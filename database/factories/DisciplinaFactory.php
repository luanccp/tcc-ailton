<?php

use Faker\Generator as Faker;

$factory->define(App\Disciplina::class, function (Faker $faker) {

    $areas = array(
      'Computação',
      'Eletronica',
      'Matemática',
      'Fisica',
    );

    return [
        'disc_codigo' =>str_random(5),
        'disc_descricao' => $faker->catchPhrase,
        'disc_area' => $areas[array_rand($areas)],
        'disc_periodo' => rand(1,10),
        'disc_chs' =>rand(40,80),
        'disc_optativa' =>rand(0,1),
        'disc_indice_reprovacao' =>str_random(2),
        'departamento_id' => rand(1,5),
    ];
});
